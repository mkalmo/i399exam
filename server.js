'use strict';

const httpGet = require('./util/http-request').httpGet;
const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());
app.use(express.static('./'));

app.get('/api/...', handler);

app.listen(3000, () => console.log('running at: http://localhost:3000'));

function handler(request, response) {
    response.end('ok');
}
