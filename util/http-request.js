
const request = require('request');

module.exports = {

    httpGet: function(url) {
        return new Promise((resolve, reject) => {
            request(url, function(error, response, body) {
                if (error) {
                    reject(error);
                } else if (response.statusCode >= 200 && response.statusCode < 300) {
                    resolve(JSON.parse(body));
                } else {
                    reject(body);
                }
            });
        });
    }
};


